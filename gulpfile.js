var gulp = require('gulp');
var imagemin = require('gulp-imagemin');
var cleanCss= require('gulp-clean-css');}
var uglify = require('gulp-uglify');
var auto = require('gulp-autoprefixer');
var browserSync = require('browser-sync');
var runSequence = require('run-sequence');

gulp.task('minificar-css', function() {
 gulp.src('src/css/*.css')
   .pipe( cleanCss())
   .pipe( auto())
   .pipe( gulp.dest('public/css'))
});

gulp.task('minificar-imagenes', function() {
 gulp.src('src/images/*')
   .pipe( imagemin())
   .pipe( gulp.dest('public/images'))
});

gulp.task('minificar-js', function() {
 gulp.src('src/js/*.js')
   .pipe( uglify())
   .pipe( gulp.dest('public/js'))
});

gulp.task( 'browser-sync', function() {
 var config = {
   startPath: 'index.html',
   notify: false,
   server:{
     baseDir: 'public'
   }
 };

 browserSync.init(config);
});

gulp.task('browser-reload', function () {
 browserSync.reload();
})

gulp.task('default', function() {
 runSequence(
   ['browser-sync'],
   ['minificar-imagenes'],
   ['minificar-css'],
   ['minificar-js']
 );

gulp.watch(['src/*', 'src/**/*'], function() {
 runSequence(
   ['minificar-imagenes'],
   ['minificar-css'],
   ['minificar-js'],
   ['browser-reload']
 );
 });
});
